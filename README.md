# This Readme covers a simple example for the DEOS EDGE API
The DeviceAPI is uses on GraphQL (good read: https://graphql.org/learn/).
A short introduction and comparison to REST can be found [here - DE](https://www.hosteurope.de/blog/graphql-api-abfragesprache-alternative-zu-restful-web-services/?utm_source=Facebook&utm_medium=social&utm_campaign=191019%2Fgraphql-api-abfragesprache-alternative-zu-restful-web-services%2F)

A GraphQL API is self-documenting, and can be easily tested via GraphQL Playground on the EDGE System by calling the respective API Endpoint in a web-browser, using http://demo.edomo.de/graphql

In these you can also see the "schema" of the API which tells what can be done with which query, mutation, what's the payload and objects.

Alternatively you can also use POSTMAN https://www.getpostman.com/downloads/ which has a first support for GrqphQL.
A collection for our devices API can be found [here](/Devices.postman_collection.json)

#### Endpoints:
* API=http://demo.edomo.de/graphql
* SUBSCRIPTIONS=wss://demo.edomo.de/graphql
* User: PLEASE REQUEST
* Password: PLEASE REQUEST


## Authorization:

Authorization works via HTTP Request-HEADER `Bearer`: 

`
Authorization Bearer eyJhbGciOiJIUzI1....
`
in GraphQL Playground you can use this in the lower "tab" HTTP-Header:
```javascript
{"authorization": "bearer XXXXXXXXXXXXX"}
```

To retrieve the Bearer, a signin mutation needs to be posted, returning a Bearer Token:

```javascript
mutation {
  signInUser(data: { username: "MY_USER", password: "MY_PASSWORD" }) {
    token
    features
  }
}

```

Result:
```javascript
{
  "data": {
    "signInUser": {
      "token": "XXXXXXXXXXXXX",
      "features": []
    }
  }
}
```

## Retrieve and Change (Mutate) Values/Datapoints in Devices

### Retrieve Devices

```javascript
query {
  devices {
    id
    name
    services {
      type
      characteristics {
        id
        name
      }
    }
  }
}
```

```javascript
Result:
{
    "data": {
        "devices": [
            {
                "id": "VBRdjAOgCo",
                "name": "Temperatur A.4.2.2",
                "services": [
                    {
                        "type": "TemperatureSensor",
                        "characteristics": [
                            {
                                "id": "EAkhu78x2w",
                                "name": "Temperatur Küche"
                            }
                        ]
                    }
                ]
            },
            {
                "id": "OPHrh0Lt9h",
                "name": "Feuchte A.4.2.2",
                "services": [
                    {
                        "type": "HumiditySensor",
                        "characteristics": [
                            {
                                "id": "bJpj0SLeAo",
                                "name": "Feuchtigkeit Küche"
                            }
                        ]
                    }
                ]
            },
            {
                "id": "GBwDD$yztu",
                "name": "Rollo A.4.2.2",
                "services": [
                    {
                        "type": "WindowCovering",
                        "characteristics": [
                            {
                                "id": "nFro7cMlMY",
                                "name": "Rollo Küche"
                            },
                            {
                                "id": "xf17grA6ZA",
                                "name": null
                            },
                            {
                                "id": "6lFpzUPu1o",
                                "name": null
                            }
                        ]
                    }
                ]
            }
        ]
    }
}
```

#### Read and mutate a specific device

Reading the services and following characteristics of a shutter device:

```javascript
query {
  device(where: { id: "TeGbAJcfa" }) {
    id
    name
    services {
      type
      characteristics {
        id
        name
        value
        type
        description
        readonly
      }
    }
  }
}


```

Result:
```javascript
{
    "data": {
        "device": {
            "id": "q@5vmv9n1",
            "name": "Rollo A.4.2.2",
            "services": [
                {
                    "type": "WindowCovering",
                    "characteristics": [
                        {
                            "id": "sqa2@Fnw8",
                            "name": null,
                            "value": false,
                            "type": "ActivePower",
                            "description": "Motor power",
                            "readonly": false
                        },
                        {
                            "id": "bDJzNsbf6",
                            "name": null,
                            "value": "{\"name\":\"Closing\"}",
                            "type": "Other",
                            "description": "Motor status",
                            "readonly": false
                        },
                        {
                            "id": "p@ONNmAD9",
                            "name": null,
                            "value": 50,
                            "type": "Level",
                            "description": "Blinds' position",
                            "readonly": false
                        }
                    ]
                }
            ]
        }
    }
}
```


### Change its "value"
Be aware, that only values with    `"readonly": false` can be updated.


```javascript
mutation {
  requestDeviceServiceCharacteristicValueUpdate(
    where: { id: "nFro7cMlMY" }
    data: { value: true }
  )
}

```

## Subscribing to Updates

To subscibe for changes use the following `subscription`, GraphQl Playground  will then subscribe to `SUBSCRIPTIONS Websocket`

Every change for every device can be seen with:
```javascript
subscription {
  deviceServiceCharacteristic {
    node {
      id
      name
      value
    }
  }
}

```

Result:
```javascript
{
  "data": {
    "deviceServiceCharacteristic": {
      "node": {
        "id": "nFro7cMlMY",
        "name": "Rollo Küche",
        "value": false
      }
    }
  }
}

....

{
  "data": {
    "deviceServiceCharacteristic": {
      "node": {
        "id": "nFro7cMlMY",
        "name": "Rollo Küche",
        "value": true
      }
    }
  }
}
```
